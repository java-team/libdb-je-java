/*-
 * See the file LICENSE for redistribution information.
 *
 * Copyright (c) 2002,2010 Oracle.  All rights reserved.
 *
 * $Id: InternalException.java,v 1.19.2.2 2010/01/04 15:30:38 cwl Exp $
 */

package com.sleepycat.je.utilint;

import com.sleepycat.je.DatabaseException;

/**
 * Some internal inconsistency exception.
 */
public class InternalException extends DatabaseException {

    public InternalException() {
	super();
    }

    public InternalException(String message) {
	super(message);
    }
}
