/*-
 * See the file LICENSE for redistribution information.
 *
 * Copyright (c) 2002,2010 Oracle.  All rights reserved.
 *
 * $Id: DatabaseStats.java,v 1.24.2.2 2010/01/04 15:30:27 cwl Exp $
 */

package com.sleepycat.je;

import java.io.Serializable;

/**
 * Statistics for a single database.
 */
public abstract class DatabaseStats implements Serializable {
    // no public constructor
    protected DatabaseStats() {}
}
