/*-
 * See the file LICENSE for redistribution information.
 *
 * Copyright (c) 2002,2010 Oracle.  All rights reserved.
 *
 * $Id: ReplicaConsistencyPolicy.java,v 1.3.2.2 2010/01/04 15:30:27 cwl Exp $
 */

package com.sleepycat.je;

import com.sleepycat.je.dbi.ReplicatorInstance;

/**
 * @hidden
 * Feature not yet available.
 *
 * The interface for Consistency policies used to provide consistency
 * guarantees at a Replica. A transaction initiated at a replica will wait in
 * the Environment.beginTransaction method until the required consistency
 * policy is satisfied.
 */
public interface ReplicaConsistencyPolicy {

    /**
     * Ensures that the replica is within the constraints specified by this
     * policy. If it isn't the method waits until the constraint is satisfied
     * by the replica.
     */
    public void ensureConsistency(ReplicatorInstance repInstance)
        throws InterruptedException, DatabaseException;
}
